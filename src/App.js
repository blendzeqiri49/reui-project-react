import { Container } from "./components/Container/Container";
import { Services } from "./components/Services/Services";
import { Collections } from "./sections/Collections/Collections";
import { FooterWrapper } from "./sections/FooterWrapper/FooterWrapper";
import { Gallery } from "./sections/Gallery/Gallery";
import { HalfSection } from "./sections/HalfSection/HalfSection";

import { Header } from "./sections/Header/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <Container>
        <Services />

        <HalfSection />
        <Gallery />
        <Collections />
      </Container>
      <FooterWrapper />
    </div>
  );
}

export default App;
