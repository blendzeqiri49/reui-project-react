import React from "react";

import Room1 from "../../assets/images/123.png";
import Room2 from "../../assets/images/456.png";
import Room3 from "../../assets/images/789.png";
import { Button } from "../../components/Button/Button";
import { Heading } from "../../components/Heading/Heading";

import "./Collections.css";

export const Collections = () => {
  return (
    <div className="Collections">
      <Heading title="Collections" />
      <div className="collections-wrapper">
        <div className="collections-wrapper__text">
          <div className="collections-wrapper__description">
            <p className="collection-info">
              Housesale chose to work together with the Lebanese designer Nada
              Debs. We are thrilled to present this collaboration and the LJUV
              collection, and we hope that you can see, feel, use the connection
              between a designer from the Middle East democratic design.
            </p>
            <p className="collection-info">
              Nada Debs is a Levantine designer living and working in Beirut.
              Her work spans scale and discipline: from product to furniture
              design to one-off commission across craft, art, fashion, and
              interiors. Nada grew up in Japan, studied design in Rhode Island
              School of Design in the US, and has spent significant periods
              living and traveling the world, finding connections of each of her
              experiences in all her work. She calls her approach handmade, and
              heart-made!
            </p>
          </div>
          <Button />
        </div>
        <div className="collections-images">
          <img src={Room1} alt="Room" />
          <img src={Room2} alt="Room" />
          <img src={Room3} alt="Room" />
        </div>
      </div>
    </div>
  );
};
