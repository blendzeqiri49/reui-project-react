import React, { useState } from "react";
import { Container } from "../../components/Container/Container";
import Logo from "../../assets/images/HOUSESALE.svg";
import cs from "classnames";

import "./Header.css";
export const Header = () => {
  const [toggled, setToggle] = useState(false);
  const classes = cs("header-navigation", {
    "header-navigation--open": toggled,
  });
  const hamburger = cs("hamburger", {
    active: toggled,
  });

  const hamburgerIcon = (e) => {
    setToggle((previous) => {
      // document.body.classList.add("modal-open");
      return !previous;
    });
  };
  return (
    <div className="Header-wrapper">
      <div className="Header-wrapper__bg-image">
        <Container>
          <div className="header-container">
            <div className="header-wrapper">
              <div className="logo">
                <a href="About.html">
                  <img src={Logo} alt="Logo e projektit" />
                </a>
              </div>
              <div class={hamburger} onClick={hamburgerIcon}>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <div id="header-navigation" className={classes}>
                <ul className="header-navigation__list">
                  <li>
                    <a href="About.html">On sale</a>
                  </li>
                  <li>
                    <a href="About.html">For rent</a>
                  </li>
                  <li>
                    <a href="About.html">New Residential Project</a>
                  </li>
                  <li>
                    <a href="About.html">Property News</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="header-content">
              <h1 className="header-content__title">
                Quality home services, on demand
              </h1>
              <p className="header-content__desc">
                Experienced, hand-picked Professionals to serve you at your
                doorstep
              </p>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
};
