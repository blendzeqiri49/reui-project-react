import React from "react";
import Image from "../../assets/images/image1.jpg";
import Group1 from "../../assets/images/Group1.svg";
import Group2 from "../../assets/images/Group2.svg";
import Group3 from "../../assets/images/Group3.svg";

import "./HalfSection.css";
import { Heading } from "../../components/Heading/Heading";

export const HalfSection = () => {
  return (
    <div className="half-section">
      <div className="half-section__info">
        <Heading title="Why HOUSESALE Company?" />
        <div className="half-section__info-item">
          <div className="info-image">
            <img src={Group1} alt="foto" />
          </div>
          <div className="half-section__desc">
            <h3>Transparent pricing</h3>
            <p>See fixed prices before you book. No hidden charges.</p>
          </div>
        </div>

        <div className="half-section__info-item">
          <div className="info-image">
            <img src={Group2} alt="foto" />
          </div>
          <div className="half-section__desc">
            <h3>Experts only</h3>
            <p>Our professionals are well trained and have on-job expertise.</p>
          </div>
        </div>

        <div className="half-section__info-item">
          <div className="info-image">
            <img src={Group3} alt="foto" />
          </div>
          <div className="half-section__desc">
            <h3>Fully equipped</h3>
            <p>We bring everything needed to get the job done well.</p>
          </div>
        </div>
      </div>

      <div className="half-section__image">
        <span className="half-section__rectangle"></span>
        <img src={Image} alt="section " />
      </div>
    </div>
  );
};
